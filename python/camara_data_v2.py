"""
31-03-2022

@author: Lucas Mancini <mancinilucas95@gmail.com>

"""

from datetime import datetime
import os
import serial
import time
import threading

#%% INFO: THERSEAM EXPERIMENT SERIAL TO CSV LOGGER

#<experiment cmd><space><params separated with coma><space>\n

#dip_exp_1 id,velocidad,aceleracion,desplazamiento,delta_total \n
#dip_exp_2 id,velocidad,aceleracion,desplazamiento,delta_total,delta_acc,delta_dec \n
    
    
#%% MACROS AND GLOBAL VARIABLES

#SERIAL MACROS
SERIAL_PORT = '/dev/ttyUSB0'
SERIAL_BAUDRATE = 115200

#FILE ID
# el id puede ser un numero o el tiempo en que se inicializo
FILE_ID_NUM = str(datetime.now().date()) 
#FILE_ID_NUM = str(datetime.now().date())+"-"+str(datetime.now().hour)+"h"+str(datetime.now().minute)+"m" 

# Filepaths with formateable date
FILE_PATH_THERSEAM_EXP1 = 'data/camara_exp1_{}.csv'

therseam_exp1_keyword = 'camara_exp_1'
therseam_exp1_labels = 'timer,hum1,hum2'

#%%  Serial Config

ser = serial.Serial()
ser.baudrate = SERIAL_BAUDRATE
ser.port = SERIAL_PORT


#%% INTERNAL FUNCTIONS

def write_msg_in_file(row, file_path):
    # open the file in the write mode
    with open(file_path, 'a',newline='') as f:
        f.write(row + '\n')
    return

def save_msg(msg):
      
    # saves message according to the content
    if msg[0] == therseam_exp1_keyword:
        # saves data
        write_msg_in_file(msg[1], FILE_PATH_THERSEAM_EXP1.format(FILE_ID_NUM))
        
        

#%% MAIN DEF
def main_init():
    try:          
        #FILES INIT
        try:
            path = [FILE_PATH_THERSEAM_EXP1.format(FILE_ID_NUM)]
            if not os.path.exists(path[0]):
                write_msg_in_file(therseam_exp1_labels, path[0])
        except:
            print("ERROR: file init fail")
            raise

        #SERIAL COM INIT
        try:
            ser.close()
            ser.open()
            ser.reset_input_buffer()
            ser.is_open     
        except:
            print("ERROR: serial init fail")
        return True
    except:
        return False

def read_from_microcontroller():
    
#    if ser.is_open:
        while True:
            msg = ser.read_until(size = 96).decode('UTF-8')
            if msg:
                try:
                    data = msg.split(sep = " ")
                    save_msg(data)
                except:
                    print("ERROR: unpacking")        
            
 #   else:
  #      print("STATUS: Communication close")
 #       return False    
    
    
    
# Function to send commands to the microcontroller
def send_to_microcontroller():
    while True:
        # Get a command from the user, spyder console
        command = input('ins-tecsci-chamber: ')

        # Send the command to the microcontroller
        ser.write(command.encode('utf-8'))

        
       
    
def main_deinit():
    ser.close()
    return True

#%% EXECUTION


if __name__ == "__main__":
#    if( main_init() == True):
#        print("Main init SUCCESS")
#        if( main_loop() == False):
#            main_deinit()
#            print("Main deinit")
#    else:
#        print("Main init FAILED")
# Keep the main thread running
    main_init()   
    # Start the read and send threads
    read_thread = threading.Thread(target=read_from_microcontroller)
    read_thread.start()

    send_thread = threading.Thread(target=send_to_microcontroller)
    send_thread.start()  
    
    while True:
        time.sleep(1)
    






                 