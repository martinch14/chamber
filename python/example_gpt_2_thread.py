#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 24 13:10:20 2023

@author: martin
"""

import threading
import queue
import time

def productor(q):
    for i in range(5):
        mensaje = f"Mensaje {i}"
        q.put(mensaje)
        print(f"[Productor] Mensaje enviado: {mensaje}")
        time.sleep(1)
    q.put(None)  # Marcar el final de los mensajes

def consumidor(q):
    while True:
        mensaje = q.get()
        if mensaje is None:
            break
        print(f"[Consumidor] Mensaje recibido: {mensaje}")

# Crear una cola compartida
q = queue.Queue()

# Crear los hilos
hilo_productor = threading.Thread(target=productor, args=(q,))
hilo_consumidor = threading.Thread(target=consumidor, args=(q,))

# Iniciar los hilos
hilo_productor.start()
hilo_consumidor.start()

# Esperar a que los hilos terminen
hilo_productor.join()
hilo_consumidor.join()

print("Programa finalizado")