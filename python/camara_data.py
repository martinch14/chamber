"""
31-03-2022

@author: Lucas Mancini <mancinilucas95@gmail.com>

"""

from datetime import datetime
import os
import serial

#%% INFO: THERSEAM EXPERIMENT SERIAL TO CSV LOGGER

#<experiment cmd><space><params separated with coma><space>\n

#dip_exp_1 id,velocidad,aceleracion,desplazamiento,delta_total \n
#dip_exp_2 id,velocidad,aceleracion,desplazamiento,delta_total,delta_acc,delta_dec \n
    
    
#%% MACROS AND GLOBAL VARIABLES

#SERIAL MACROS
SERIAL_PORT = '/dev/ttyUSB0'
SERIAL_BAUDRATE = 115200

#FILE ID
# el id puede ser un numero o el tiempo en que se inicializo
FILE_ID_NUM = str(datetime.now().date()) 
#FILE_ID_NUM = str(datetime.now().date())+"-"+str(datetime.now().hour)+"h"+str(datetime.now().minute)+"m" 

# Filepaths with formateable date
FILE_PATH_THERSEAM_EXP1 = 'data/camara_exp1_{}.csv'

therseam_exp1_keyword = 'camara_exp_1'
therseam_exp1_labels = 'timer,enable,temp_target,temp_actual,delta_t,dac_v,rele_c'

#%%  Serial Config

ser = serial.Serial()
ser.baudrate = SERIAL_BAUDRATE
ser.port = SERIAL_PORT


#%% AUXILIAR FUNCTIONS

def write_msg_in_file(row, file_path):
    # open the file in the write mode
    with open(file_path, 'a',newline='') as f:
        f.write(row + '\n')
    return

def save_msg(msg):
      
    # saves message according to the content
    if msg[0] == therseam_exp1_keyword:
        # saves data
        write_msg_in_file(msg[1], FILE_PATH_THERSEAM_EXP1.format(FILE_ID_NUM))
  
#%% MAIN DEF
def main_init():
    try:          
        #FILES INIT
        try:
            path = [FILE_PATH_THERSEAM_EXP1.format(FILE_ID_NUM)]
            if not os.path.exists(path[0]):
                write_msg_in_file(therseam_exp1_labels, path[0])
        except:
            print("ERROR: file init fail")
            raise

        #SERIAL COM INIT
        try:
            ser.close()
            ser.open()
            ser.reset_input_buffer()
            ser.is_open     
        except:
            print("ERROR: serial init fail")
            raise
        return True
    except:
        return False

def main_loop():
    if ser.is_open:
        while True:
            #msg = ser.read_until(size = 96).decode('UTF-8')
            #if msg:
            #    try:
            #        data = msg.split(sep = " ")
            #        print(data)
            #        save_msg(data)
            #    except:
            #        print("ERROR: unpacking")
                    
            
           if ser.in_waiting > 0:
               # Leer los datos recibidos
               data = ser.readline().decode().rstrip()
               print('Recibido:', data)
        
               # Enviar una respuesta
               respuesta = 'Mensaje recibido: {}'.format(data)
               ser.write(respuesta.encode())
               print('Enviado:', respuesta) 
                
            
    else:
        print("STATUS: Communication close")
        return False
    
def main_deinit():
    ser.close()
    return True

#%% EXECUTION

if __name__ == "__main__":
    if(True == main_init()):
        print("Main init SUCCESS")
        if(False == main_loop()):
            main_deinit()
            print("Main deinit")
    else:
        print("Main init FAILED")

                 