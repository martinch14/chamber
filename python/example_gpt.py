#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 24 11:11:52 2023

@author: martin
"""

import serial
import time
import  threading

# Set up the serial connection
ser = serial.Serial('/dev/ttyACM1', 115200, timeout=1)

# Function to read from the microcontroller
def read_from_microcontroller():
    while True:
        # Wait for data to be available
        if ser.in_waiting > 0:
            # Read the data and print it
            data = ser.readline().decode('utf-8').rstrip()

# Function to send commands to the microcontroller
def send_to_microcontroller():
    while True:
        # Get a command from the user
        command = input('Enter a command: ')

        # Send the command to the microcontroller
        ser.write(command.encode('utf-8'))

        # Wait for a response from the microcontroller
        response = ser.readline().decode('utf-8').rstrip()
        print('Received response:', response)

# Start the read and send threads
read_thread = threading.Thread(target=read_from_microcontroller)
read_thread.start()

send_thread = threading.Thread(target=send_to_microcontroller)
send_thread.start()

# Keep the main thread running
while True:
    time.sleep(1)