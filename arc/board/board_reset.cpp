/********************** inclusions *******************************************/

#include "Arduino.h"
#include "board_log.hpp"
#include "board_reset.hpp"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

static struct
{
    int reset_source;
} self_;

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

static void save_reset_source_(void)
{
    switch (esp_reset_reason())
    {
        case ESP_RST_POWERON:
            self_.reset_source = BOARD_RESET_SOURCE_POWERON;
            break;
        case ESP_RST_WDT:
            self_.reset_source = BOARD_RESET_SOURCE_WDT;
            break;
        case ESP_RST_DEEPSLEEP:
            self_.reset_source = BOARD_RESET_SOURCE_DEEPSLEEP;
            break;
        case ESP_RST_SW:
            self_.reset_source = BOARD_RESET_SOURCE_SW;
            break;
        default:
            self_.reset_source = BOARD_RESET_SOURCE_UNKNOWN;
            break;
    }
}

/********************** external functions definition ************************/

int board_reset_get_source(void)
{
    return self_.reset_source;
}

void board_reset_restart(void)
{
    esp_restart();
}

void board_reset_init(void)
{
    BOARD_LOG_TRACE_FUNCTION();
    save_reset_source_();

}
void board_reset_deinit(void)
{

}
void board_reset_loop(void)
{

}

/********************** end of file ******************************************/
