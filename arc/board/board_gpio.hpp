#ifndef ARC_BOARD_BOARD_GPIO_HPP_
#define ARC_BOARD_BOARD_GPIO_HPP_

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>

#include "hardware.hpp"

/********************** macros ***********************************************/

/********************** typedef **********************************************/

typedef enum {

	BOARD_GPIOIN_STOP = 		HARDWARE_ESP32_GPIOIN_3,
	BOARD_GPIOIN_HEATENABLE = 	HARDWARE_ESP32_GPIOIN_4,
	BOARD_GPIOIN_EMERGENCY= 	HARDWARE_ESP32_GPIOIN_5,



} board_gpio_in_t;

typedef enum {

	BOARD_GPIOOUT_DRY_VALVE = 	HARDWARE_ESP32_GPIO_7,
	BOARD_GPIOOUT_WET_VALVE = 	HARDWARE_ESP32_GPIO_6,
	BOARD_GPIOOUT_LED 		= 	HARDWARE_ESP32_GPIO_0,

} board_gpio_out_t;

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void board_gpio_init();
void board_gpio_deinit();
void board_gpio_loop();

void board_gpio_output_setup(board_gpio_out_t out);
void board_gpio_input_setup(board_gpio_in_t in);
bool board_gpio_read(board_gpio_in_t in);
void board_gpio_write(board_gpio_out_t out, bool state); //0 low - 1 high

#endif /* ARC_BOARD_BOARD_GPIO_H_ */
/********************** end of file ******************************************/
