/*
 * Copyright (c) 2020 Sebastian Bedin <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @file	board_log.hpp
 * @date	2020-14-10
 * @author:
 *  - SB		Sebastian Bedin <sebabedin@gmail.com> (api_log.hpp)
 *  - LM		Lucas Mancini <mancinilucas95@gmail.com>
 * @version	v1.0.0
 *
 *	Date		Author		Modification
 * 	23-10		SB			Refactor and rename to .cpp (api_log.hpp)
 * 	30-10		LM			Modification from api_log to board_log
 */

#ifndef BOARD_LOG_HPP_
#define BOARD_LOG_HPP_

/********************** inclusions *******************************************/

#include <stdio.h>
#include "board_serial.hpp"

/********************** macros ***********************************************/

#define BOARD_LOG_ENABLE			(0)
#define BOARD_LOG_PRINT_BUFFER_LEN	(128)
#define BOARD_LOG_HEAD 				("/board/: ")
#define BOARD_LOG_NL 				("\n")
#define BOARD_LOG_SERIAL 			(BOARD_UART_0)

#define BOARD_LOG_PRINT(...)\
	sprintf(board_log_buffer(), __VA_ARGS__);\
	board_log_print_buffer()

#define BOARD_LOG_PRINT_NL()			BOARD_LOG_PRINT(BOARD_LOG_NL)

#define BOARD_LOG_FORMAT(head, ...)\
	BOARD_LOG_PRINT(head);\
	BOARD_LOG_PRINT(__VA_ARGS__);\
	BOARD_LOG_PRINT_NL();

#if 1 == BOARD_LOG_ENABLE
#define BOARD_LOG(...) BOARD_LOG_FORMAT(BOARD_LOG_HEAD, __VA_ARGS__);
#else
#define BOARD_LOG(...)
#endif

#if 1 == BOARD_LOG_ENABLE
#define BOARD_LOG_TRACE_FUNCTION() BOARD_LOG("%s", __FUNCTION__);
#else
#define BOARD_LOG_TRACE_FUNCTION(...)
#endif

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void board_log_print(char* p_text);

void board_log_print_buffer(void);

char* board_log_buffer(void);

void board_log_enable(bool value);

void board_log_init(void);

void board_log_deinit(void);

void board_log_loop(void);

#endif /* BOARD_LOG_HPP_ */
/********************** end of file ******************************************/
