/*
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		board_spiffs.c
* @date		30 sep. 2021
* @author	
*		-Lucas Mancini   <mancinilucas95@gmail.com>
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/

#include "board_spiffs.hpp"
#include "board_log.hpp"

#include "esp_spiffs.h"
#include "esp_log.h"
#include "string.h"
#include "stdio.h"
#include "stdlib.h"

/********************** macros and definitions *******************************/

#define BOARD_SPIFFS_MAX_FILES_SIMULT (5)
#define BOARD_SPIFFS_FORMAT_IF_FAILS_MOUNTING (true)

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

esp_vfs_spiffs_conf_t conf;
static const char *TAG = "board_spiffs";


/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/

void board_spiffs_config(const char * path){

	//CONFIG
    conf.base_path = path;
    conf.partition_label = NULL;
	conf.max_files = BOARD_SPIFFS_MAX_FILES_SIMULT;
	conf.format_if_mount_failed = BOARD_SPIFFS_FORMAT_IF_FAILS_MOUNTING;

}

bool board_spiffs_mount(){


    // Use settings defined above to initialize and mount SPIFFS filesystem.
    // Note: esp_vfs_spiffs_register is an all-in-one convenience function.
    esp_err_t ret = esp_vfs_spiffs_register(&conf);

    if (ret != ESP_OK) {
        if (ret == ESP_FAIL) {
            ESP_LOGE(TAG, "Failed to mount or format filesystem");
        } else if (ret == ESP_ERR_NOT_FOUND) {
            ESP_LOGE(TAG, "Failed to find SPIFFS partition");
        } else {
            ESP_LOGE(TAG, "Failed to initialize SPIFFS (%s)", esp_err_to_name(ret));
        }
        return false;
    }
    else{
    	ESP_LOGI(TAG, "Mounted filesystem");
    }

    //INFO
    size_t total = 0, used = 0;
    ret = esp_spiffs_info(conf.partition_label, &total, &used);
    if (ret != ESP_OK) {
        ESP_LOGE(TAG, "Failed to get SPIFFS partition information (%s)", esp_err_to_name(ret));
        return false;
    } else {
        ESP_LOGI(TAG, "Partition size: total: %d, used: %d", total, used);
    }

    return true;

}

void board_spiffs_unmount(){
    // All done, unmount partition and disable SPIFFS
    esp_vfs_spiffs_unregister(conf.partition_label);
}

bool board_spiffs_is_mounted(void){

	return esp_spiffs_mounted(conf.partition_label);
}

FILE* board_spiffs_open(const char * path, const char* mode){

    FILE* f = fopen(path, mode);
    if (f == NULL) {
        ESP_LOGE(TAG, "Failed to open file");
    }

    return f;
}

void board_spiffs_close(FILE* f){
    fclose(f);
}


void board_spiffs_write(FILE* f, const char * message){

    fprintf(f, "%.*s", strlen(message), message);

}

bool board_spiffs_read_line(FILE* f, char * buff, uint32_t size){

	if(NULL == fgets(buff, size, f)){
		return false;
	}

    char* pos = strchr(buff, '\n');
    if (pos) *pos = '\0';

	return true;
}

bool board_spiffs_read_n_line(FILE* f, char * buff, uint32_t size, uint32_t line_n){

	if(!f){
		return false;
	}

	fseek(f,0,SEEK_SET);

	for(int i=0; i < (line_n+1); i++){

		if(NULL == fgets(buff, size, f)){
			return false;
		}

	}

	return true;
}

bool board_spiffs_delete_file(const char * path){

	if(0 == remove(path)){
		return true;
	}

   return false;
}

bool board_spiffs_rename_file(const char * path_old,const char * path_new){

	if(0 == rename( path_old,path_new)){
		return true;
	}

  return false;

}



void board_spiffs_init(){

	board_spiffs_config("/spiffs");
	board_spiffs_mount();

	if (true == board_spiffs_is_mounted()){
		BOARD_LOG(TAG,"Mounted!");
	}


	BOARD_LOG_TRACE_FUNCTION();
}
void board_spiffs_deinit(){

}
void board_spiffs_loop(){

}


/********************** end of file ******************************************/

/** @}Final Doxygen */
