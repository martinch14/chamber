/* 
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>  
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		boad_i2c.hpp
* @date		22 sep. 2021
* @author	
*		-Martin Abel Gambarotta   <magambarotta@gmail.com>
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

#ifndef COMPONENTS_BOARD_INCLUDE_BOARD_I2C_H_
#define COMPONENTS_BOARD_INCLUDE_BOARD_I2C_H_
/********************** inclusions *******************************************/
#include "driver/i2c.h"
#include "esp_log.h"
#include "stdint.h"
/********************** macros ***********************************************/

typedef enum{

	BOARD_I2C_0=0,
	BOARD_I2C_1=1,
	BOARD_I2C_MAX

}board_i2c_id_t;

/********************** typedef **********************************************/

/********************** external data declaration ****************************/
/********************** external functions declaration ***********************/


void board_i2c_transmission_begin(board_i2c_id_t i2c_n, int address);
void board_i2c_transmission_end(board_i2c_id_t i2c_n);


uint8_t board_i2c_master_write(board_i2c_id_t i2c_n,uint8_t dev_address,uint8_t reg_addr, uint8_t *data, uint32_t data_len);
uint8_t board_i2c_master_read(board_i2c_id_t i2c_n,uint8_t dev_address,uint8_t reg_addr,uint8_t *data, uint32_t data_len);

void board_i2c_init(void);
void board_i2c_deinit(void);
void board_i2c_loop(void);

#endif /* COMPONENTS_BOARD_INCLUDE_BOARD_I2C_H_ */
/** @}Final Doxygen */
