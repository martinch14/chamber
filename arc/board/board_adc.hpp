/*
 * Copyright (c) 2020 Lucas Mancini <mancinilucas95@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @file	board_adc.hpp
 * @date	2020-16-10
 * @author:
 *  - Lucas Mancini <mancinilucas95@gmail.com>
 * @version	v1.0.0
 */

#ifndef BOARD_ADC_HPP_
#define BOARD_ADC_HPP_

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include "hardware.hpp"

/********************** macros ***********************************************/

#define BOARD_ADC_0_PIN BOARD_LAYOUT_ADC_0_PIN
#define BOARD_ADC_1_PIN BOARD_LAYOUT_ADC_1_PIN

#define BOARD_ADC_READ_RESOLUTION BOARD_LAYOUT_ADC_READ_RESOLUTION
#define BOARD_ADC_REFERENCE_VOLTAGE BOARD_LAYOUT_ADC_REFERENCE_VOLTAGE

//values from other poryect
#define BOARD_ADC_CALIBRATION_SLOPE		1//0.975
#define BOARD_ADC_CALIBRATION_OFFSET	0//0.135

/********************** typedef **********************************************/

typedef enum {

	BOARD_ADC_TAG_1 = BOARD_ADC_0_PIN,
	BOARD_ADC_TAG_2=  BOARD_ADC_1_PIN,

} board_adc_tag_t;

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

float board_adc_read_voltage_raw(board_adc_tag_t adc_tag);
float board_adc_read_voltage_to_calibrate(board_adc_tag_t adc_tag, float offset, float slope);
float board_adc_read_voltage_calibrated(board_adc_tag_t adc_tag);
uint16_t board_adc_read_bits(board_adc_tag_t adc_tag);

void board_adc_init(void);
void board_adc_deinit(void);
void board_adc_loop(void);

#endif /* BOARD_ADC_HPP_ */
/********************** end of file ******************************************/
