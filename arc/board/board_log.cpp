/*
 * Copyright (c) 2020 Sebastian Bedin <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @file	board_log.cpp
 * @date	2020-14-10
 * @author:
 *  - SB		Sebastian Bedin <sebabedin@gmail.com>
 *  - LM		Lucas Mancini <mancinilucas95@gmail.com>
 * @version	v1.0.0
 *
 *	Date		Author		Modification
 * 	23-10		SB			Refactor and rename to .cpp (api_log.cpp)
 * 	30-10		LM			Modification from api_log to board_log
 */

/********************** inclusions *******************************************/

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "board_log.hpp"


/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

static bool g_enable_;

/********************** external data definition *****************************/

static char g_buffer_[BOARD_LOG_PRINT_BUFFER_LEN];

/********************** internal functions definition ************************/

/********************** external functions definition ************************/

void board_log_print(char* p_text)
{
    if(true == g_enable_)
    {
        board_serial_write(BOARD_LOG_SERIAL, p_text, strlen(p_text));
    }
}

void board_log_print_buffer(void)
{
    board_log_print(g_buffer_);
}

char* board_log_buffer(void)
{
    return g_buffer_;
}

void board_log_enable(bool value)
{
    g_enable_ = value;
}

void board_log_init(void)
{
    g_enable_ = true;
}

void board_log_deinit(void)
{

}

void board_log_loop(void)
{

}

/********************** end of file ******************************************/
