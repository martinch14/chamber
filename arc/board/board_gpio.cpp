/********************** inclusions *******************************************/
#include "board_gpio.hpp"
#include "board_log.hpp"
#include "Arduino.h"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/

void board_gpio_init(){

#if 0

	board_gpio_input_setup(BOARD_GPIOIN_START_1);
	board_gpio_input_setup(BOARD_GPIOIN_START_2);
	board_gpio_input_setup(BOARD_GPIOIN_STOP);
	board_gpio_input_setup(BOARD_GPIOIN_HEATENABLE);
	board_gpio_input_setup(BOARD_GPIOIN_EMERGENCY);
	board_gpio_input_setup(BOARD_GPIOIN_AUX_EXFC2);
	board_gpio_input_setup(BOARD_GPIOIN_AUX_EXFC3);
#endif

	board_gpio_output_setup(BOARD_GPIOOUT_DRY_VALVE);
	board_gpio_output_setup(BOARD_GPIOOUT_WET_VALVE);
	board_gpio_output_setup(BOARD_GPIOOUT_LED);

	board_gpio_write(BOARD_GPIOOUT_DRY_VALVE, false);
	board_gpio_write(BOARD_GPIOOUT_WET_VALVE, false);


    BOARD_LOG_TRACE_FUNCTION();

	return;

}
void board_gpio_deinit(){

}
void board_gpio_loop(){

}

void board_gpio_output_setup(board_gpio_out_t out) {
	pinMode(out, OUTPUT);
	return;
}

void board_gpio_input_setup(board_gpio_in_t in) {
	pinMode(in, INPUT);
	return;
}

bool board_gpio_read(board_gpio_in_t in) {
	return digitalRead(in);
}

void board_gpio_write(board_gpio_out_t out, bool state) { //0 low - 1 high
	digitalWrite(out, state);
	return;
}

/********************** end of file ******************************************/

