#ifndef BOARD_DELAY_HPP_
#define BOARD_DELAY_HPP_
/********************** inclusions *******************************************/
#include "stdint.h"
/********************** macros ***********************************************/

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void board_delay_micros(uint32_t micros);
void board_delay_milis(uint32_t milis);
void board_delay_seconds(uint32_t seconds);

void board_delay_init();
void board_delay_deinit();
void board_delay_loop();

#endif /* BOARD_DELAY_HPP_ */
/** @}Final Doxygen */
