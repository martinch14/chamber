/********************** inclusions *******************************************/
#include "board.hpp"
/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/

//Init
void board_init(void)
{
    board_log_init();
    board_serial_init();

    board_delay_init();
    board_gpio_init();
    board_reset_init();
    board_timer_init();
    board_i2c_init();

    //board_adc_init();
    board_dac_init();

    board_spiffs_init();


}


/********************** end of file ******************************************/

