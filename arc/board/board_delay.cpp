/********************** inclusions *******************************************/
#include "board_delay.hpp"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "board_log.hpp"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/

void board_delay_micros(uint32_t micros){
	vTaskDelay((micros/portTICK_PERIOD_MS)/1000);
}
void board_delay_milis(uint32_t milis){
	vTaskDelay((milis/portTICK_PERIOD_MS));
}
void board_delay_seconds(uint32_t seconds){
	vTaskDelay((seconds/portTICK_PERIOD_MS)*1000);
}


void board_delay_init(){
    BOARD_LOG_TRACE_FUNCTION();
}
void board_delay_deinit(){}
void board_delay_loop(){}

/********************** end of file ******************************************/

/** @}Final Doxygen */
