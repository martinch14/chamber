/*
 * Copyright (c) 2020 Lucas Mancini <mancinilucas95@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @file	board_adc.cpp
 * @date	2020-16-10
 * @author:
 *  - Lucas Mancini <mancinilucas95@gmail.com>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/
#include "board_adc.hpp"
#include "board_log.hpp"
#include "Arduino.h"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

#if 1 == BOARD_LAYOUT_ADC_SETSAMPLES_ENABLE
static void set_samples_(uint8_t samples)
{

    if (255 <= samples || 1 < samples)
    {
        return;
    }
    analogSetSamples(samples);

}
#endif

static void set_read_resolution_(uint8_t resolution)
{

    if (0 == resolution || 16 < resolution)
    {
        return;
    }

    analogReadResolution(resolution);

    return;
}

/********************** external functions definition ************************/


float board_adc_read_voltage_raw(board_adc_tag_t adc_tag)
{
    return (board_adc_read_bits(adc_tag)*BOARD_ADC_REFERENCE_VOLTAGE)/ pow(2, BOARD_ADC_READ_RESOLUTION);
}

uint16_t board_adc_read_bits(board_adc_tag_t adc_tag)
{
    return analogRead(adc_tag);
}

float board_adc_read_voltage_to_calibrate(board_adc_tag_t adc_tag, float offset, float slope)
{
	return board_adc_read_voltage_raw(adc_tag) / slope + offset;
}

float board_adc_read_voltage_calibrated(board_adc_tag_t adc_tag)
{
	return board_adc_read_voltage_to_calibrate(adc_tag,BOARD_ADC_CALIBRATION_OFFSET,BOARD_ADC_CALIBRATION_SLOPE);
}

void board_adc_init(void)
{
    BOARD_LOG_TRACE_FUNCTION();

    set_read_resolution_(BOARD_LAYOUT_ADC_READ_RESOLUTION);
}
void board_adc_deinit(void)
{
    BOARD_LOG_TRACE_FUNCTION();
}
void board_adc_loop(void)
{

}

/********************** end of file ******************************************/
