/*
 * Copyright (c) 2020 Lucas Mancini <mancinilucas95@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @file	board_adc.hpp
 * @date	2020-16-10
 * @author:
 *  - Lucas Mancini <mancinilucas95@gmail.com>
 * @version	v1.0.0
 */

#ifndef BOARD_DAC_HPP_
#define BOARD_DAC_HPP_

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include "hardware.hpp"

/********************** macros ***********************************************/

#define BOARD_DAC_VDD_REF BOARD_LAYOUT_DAC_VDD_REF
#define BOARD_DAC_1_ENABLE BOARD_LAYOUT_DAC_1_ENABLE
#define BOARD_DAC_2_ENABLE BOARD_LAYOUT_DAC_2_ENABLE


/********************** typedef **********************************************/

typedef enum{
	//obs: only 25 and 26 are dac pins in esp32
	BOARD_DAC_PIN_1 = BOARD_LAYOUT_DAC_1_PIN,
	BOARD_DAC_PIN_2 = BOARD_LAYOUT_DAC_2_PIN,
}board_dac_pin_tag_t;

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void board_dac_write(board_dac_pin_tag_t pin, uint8_t value);
void board_dac_write_voltage(board_dac_pin_tag_t pin, float voltage);

void board_dac_init(void);
void board_dac_deinit(void);
void board_dac_loop(void);

#endif /* BOARD_DAC_HPP_ */
/********************** end of file ******************************************/
