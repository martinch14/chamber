#ifndef RESET_HPP_
#define RESET_HPP_

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdlib.h>

/********************** macros ***********************************************/

/********************** typedef **********************************************/

enum
{
    BOARD_RESET_SOURCE_UNKNOWN,
    BOARD_RESET_SOURCE_POWERON,
    BOARD_RESET_SOURCE_WDT,
    BOARD_RESET_SOURCE_DEEPSLEEP,
    BOARD_RESET_SOURCE_SW,
};

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

int board_reset_get_source(void);
void board_reset_restart(void);

void board_reset_init(void);
void board_reset_deinit(void);
void board_reset_loop(void);

#endif /* RESET_HPP_ */
/********************** end of file ******************************************/
