/*
 * Copyright (c) 2020 Lucas Mancini <mancinilucas95@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @file	board_adc.cpp
 * @date	2020-16-10
 * @author:
 *  - Lucas Mancini <mancinilucas95@gmail.com>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include "board_dac.hpp"
#include "board_log.hpp"
#include "Arduino.h"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/
/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

static void dac_begin_(board_dac_pin_tag_t pin){
	//FOR ARDU FRAMEWORK - not begin needed

#if 0 //ESP-IDF VERSION
	if(BOARD_DAC_PIN_1 == pin){
		dac_output_enable(DAC_CHANNEL_1);
	}
	if(BOARD_DAC_PIN_2 == pin){
		dac_output_enable(DAC_CHANNEL_2);
	}
#endif

}

/********************** external functions definition ************************/

void board_dac_write(board_dac_pin_tag_t pin, uint8_t value){


	dacWrite(pin, value);


#if 0 //ESP-IDF VERSION
	if(BOARD_DAC_PIN_1 == pin){
		dac_output_voltage(DAC_CHANNEL_1, value);
	}
	if(BOARD_DAC_PIN_2 == pin){
		dac_output_voltage(DAC_CHANNEL_2, value);
	}
#endif

}

void board_dac_write_voltage(board_dac_pin_tag_t pin, float voltage){

	uint8_t value = 0;

	if(voltage > BOARD_DAC_VDD_REF){
		value = 255;
	}
	else if(voltage < 0){
		value = 0;
	}
	else{
		value = (uint8_t)(((float)voltage * (float)255.0)/ ((float)BOARD_DAC_VDD_REF));
	}

	board_dac_write(pin, value);

}

void board_dac_init(void){
    BOARD_LOG_TRACE_FUNCTION();

#if BOARD_DAC_1_ENABLE == true
    dac_begin_(BOARD_DAC_PIN_1);
#endif

#if BOARD_DAC_2_ENABLE == true
    dac_begin_(BOARD_DAC_PIN_2);
#endif




}
void board_dac_deinit(void){

}
void board_dac_loop(void){

}

/********************** end of file ******************************************/
