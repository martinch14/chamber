/*
 * Copyright (c) 2020 Lucas Mancini <mancinilucas95@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @file	board_timer.cpp
 * @date    Oct 21, 2020
 * @author:
 *  - LM        Lucas Mancini <mancinilucas95@gmail.com>
 *  - SB        Sebastian Bedin <sebabedin@gmail.com>
 * @version v2.0.0
 *
 *  Date            Author      Modification
 *  Oct 21, 2020    LM          Init
 *  Jan 27, 2021    SB          Refactor, porting x86
 */

/********************** inclusions *******************************************/

#include "board_timer.hpp"
#include "Arduino.h"
#include "board_log.hpp"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/


uint32_t board_timer_get_time_ms(void)
{
    return (uint32_t) (esp_timer_get_time() / 1000ULL);
}

void board_timer_delay_ms_blocking(uint32_t ms) {
	delay(ms);
}
void board_timer_delay_us_blocking(uint32_t us) {
	delayMicroseconds(us);
}

void board_timer_init(void)
{
    BOARD_LOG_TRACE_FUNCTION();
}

void board_timer_deinit(void)
{
}

void board_timer_loop(void)
{
}

/********************** end of file ******************************************/
