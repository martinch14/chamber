/*
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		board_serial.c
* @date		14 jun. 2021
* @author	
*		-Martin Abel Gambarotta   magambarotta@gmail.com
* @version	v1.0.0
* @brief
*
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/
#include "board_serial.hpp"
#include "driver/uart.h"
#include "esp_vfs_dev.h"
#include <HardwareSerial.h>
#include "string.h"
#include "esp_log.h"
#include "hardware.hpp"
/********************** macros and definitions *******************************/

#define BOARD_ESP32_UART_0_RX HARDWARE_ESP32_UART_0_RX
#define BOARD_ESP32_UART_0_TX HARDWARE_ESP32_UART_0_TX
#define BOARD_ESP32_UART_0_ENABLE_EVENT_QUEUE (false)
#define BOARD_ESP32_UART_0_EVENT_QUEUE_SZ (20)

#define BOARD_ESP32_UART_1_RX HARDWARE_ESP32_UART_1_RX
#define BOARD_ESP32_UART_1_TX HARDWARE_ESP32_UART_1_TX
#define BOARD_ESP32_UART_1_ENABLE_EVENT_QUEUE (true)
#define BOARD_ESP32_UART_1_EVENT_QUEUE_SZ (20)

#define BOARD_ESP32_UART_2_RX HARDWARE_ESP32_UART_2_RX
#define BOARD_ESP32_UART_2_TX HARDWARE_ESP32_UART_2_TX
#define BOARD_ESP32_UART_2_ENABLE_EVENT_QUEUE (false)
#define BOARD_ESP32_UART_2_EVENT_QUEUE_SZ (20)



#define BOARD_RX_MAX_BUF_SIZE	(256)
/********************** internal data declaration ****************************/
/********************** internal data definition *****************************/

static struct {

  uart_config_t uart_config_0,uart_config_1,uart_config_2;
  uint8_t rx_buffer[BOARD_RX_MAX_BUF_SIZE];

  QueueHandle_t event_queue_0;
  QueueHandle_t event_queue_1;
  QueueHandle_t event_queue_2;

}self;

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/

#if 0
bool board_serial_read_until(board_uart_id_t uart_id,char * caracter){
char * cadena_aux[20];

cadena_aux = Serial0.readStringUntil(caracter);

 }
#endif


bool board_serial_get_intr_queue(board_uart_id_t uart_id, QueueHandle_t* p_queuehandle){

  switch (uart_id)
    {
    case BOARD_UART_0:
			*p_queuehandle = self.event_queue_0;
			return true;
			break;
    case BOARD_UART_1:
 		 *p_queuehandle = self.event_queue_1;
 		 	return true;
    	break;
    case BOARD_UART_2:
 		 *p_queuehandle = self.event_queue_2;
 		 return true;
 		 break;
    default:
 		 *p_queuehandle = NULL;
 		 return false;
 		 break;
    }
	 return false;
}


bool board_serial_event_is_rxdata(board_serial_uart_event_t event){

	if(UART_DATA == event.type){
		return true;
	}
		return false;
}

void board_serial_setup (board_uart_id_t uart_id)
{

  switch (uart_id)
    {
    case BOARD_UART_0:

#if 0
      /* Minicom, screen, idf_monitor send CR when ENTER key is pressed */
      esp_vfs_dev_uart_port_set_rx_line_endings(CONFIG_ESP_CONSOLE_UART_NUM, ESP_LINE_ENDINGS_CR);
      /* Move the caret to the beginning of the next line on '\n' */
      esp_vfs_dev_uart_port_set_tx_line_endings(CONFIG_ESP_CONSOLE_UART_NUM, ESP_LINE_ENDINGS_CRLF);
      /* Configure UART. Note that REF_TICK is used so that the baud rate remains
       * correct while APB frequency is changing in light sleep mode.
       */
      const uart_config_t uart_config = {
              .baud_rate = 115200,
              .data_bits = UART_DATA_8_BITS,
              .parity = UART_PARITY_DISABLE,
              .stop_bits = UART_STOP_BITS_1,
              .source_clk = UART_SCLK_REF_TICK,

      };
      /* Install UART driver for interrupt-driven reads and writes */
      ESP_ERROR_CHECK( uart_driver_install(CONFIG_ESP_CONSOLE_UART_NUM,
              256, 0, 0, NULL, 0) );
      ESP_ERROR_CHECK( uart_param_config(CONFIG_ESP_CONSOLE_UART_NUM, &uart_config) );
      /* Tell VFS to use UART driver */
      esp_vfs_dev_uart_use_driver(CONFIG_ESP_CONSOLE_UART_NUM);
#endif

      /* Drain stdout before reconfiguring it */
			fflush(stdout);
			fsync(fileno(stdout));

			/* Disable buffering on stdin */
			setvbuf(stdin, NULL, _IONBF, 0);

      /* Minicom, screen, idf_monitor send CR when ENTER key is pressed */
      esp_vfs_dev_uart_port_set_rx_line_endings(UART_NUM_0, ESP_LINE_ENDINGS_CR);
      /* Move the caret to the beginning of the next line on '\n' */
      esp_vfs_dev_uart_port_set_tx_line_endings(UART_NUM_0, ESP_LINE_ENDINGS_CRLF);

      self.uart_config_0.baud_rate = 115200;
      self.uart_config_0.data_bits = UART_DATA_8_BITS;
      self.uart_config_0.parity = UART_PARITY_DISABLE;
      self.uart_config_0.stop_bits = UART_STOP_BITS_1;
      self.uart_config_0.flow_ctrl = UART_HW_FLOWCTRL_DISABLE;
      //self.uart_config_0.source_clk = UART_SCLK_APB;

      uart_driver_install (UART_NUM_0, BOARD_RX_MAX_BUF_SIZE * 2 , 0, 0, NULL, 0);
      uart_param_config (UART_NUM_0, &self.uart_config_0);
      uart_set_pin (UART_NUM_0, BOARD_ESP32_UART_0_TX, BOARD_ESP32_UART_0_RX, UART_PIN_NO_CHANGE,UART_PIN_NO_CHANGE);

      esp_vfs_dev_uart_use_driver(UART_NUM_0);

      break;

    case BOARD_UART_1:

      self.uart_config_1.baud_rate = 115200;
      self.uart_config_1.data_bits = UART_DATA_8_BITS;
      self.uart_config_1.parity = UART_PARITY_DISABLE;
      self.uart_config_1.stop_bits = UART_STOP_BITS_1;
      self.uart_config_1.flow_ctrl = UART_HW_FLOWCTRL_DISABLE;
      //self.uart_config_1.source_clk = UART_SCLK_APB;

      uart_param_config (UART_NUM_1, &self.uart_config_1);
      uart_set_pin (UART_NUM_1, BOARD_ESP32_UART_1_TX, BOARD_ESP32_UART_1_RX, UART_PIN_NO_CHANGE,UART_PIN_NO_CHANGE);

      if(true == BOARD_ESP32_UART_1_ENABLE_EVENT_QUEUE){
        uart_driver_install (UART_NUM_1, BOARD_RX_MAX_BUF_SIZE * 4, BOARD_RX_MAX_BUF_SIZE * 4, BOARD_ESP32_UART_1_EVENT_QUEUE_SZ, &self.event_queue_1, 0);
      }
      else{
      	uart_driver_install (UART_NUM_1, BOARD_RX_MAX_BUF_SIZE * 4, 0, 0, NULL, 0);
      }
      //Importante Inversion de Señales UART para comunicacion con pantalla STONE en UART 1
      uart_set_line_inverse(UART_NUM_1,UART_RXD_INV | UART_TXD_INV);

      break;

    case BOARD_UART_2:

#if 0
      //No se va a usar
      self.uart_config_2.baud_rate = 115200;
      self.uart_config_2.data_bits = UART_DATA_8_BITS;
      self.uart_config_2.parity = UART_PARITY_DISABLE;
      self.uart_config_2.stop_bits = UART_STOP_BITS_1;
      self.uart_config_2.flow_ctrl = UART_HW_FLOWCTRL_DISABLE;
      self.uart_config_2.source_clk = UART_SCLK_APB;

      uart_driver_install (UART_NUM_2, BOARD_RX_MAX_BUF_SIZE * 2, 0, 0, NULL, 0);
      uart_param_config (UART_NUM_2, &self.uart_config_2);
      uart_set_pin (UART_NUM_2, BOARD_ESP32_UART_2_TX, BOARD_ESP32_UART_2_RX, UART_PIN_NO_CHANGE,UART_PIN_NO_CHANGE);
#endif
      break;

    default:
      break;
    }
}
void board_serial_write(board_uart_id_t uart_id,void* buffer,uint32_t len)
{


   uart_write_bytes((uart_port_t)uart_id, (const char *)buffer, len);

   //int txBytes = uart_write_bytes(uart_id, buffer, len);
   //ESP_LOGI("UART_0:", "Wrote %d bytes", txBytes);

}
uint32_t board_serial_read(board_uart_id_t uart_id ,void* buffer)
{

  int rxBytes = 0;
//  uint8_t Byte;
//  int length = 0;
//
//  uart_get_buffered_data_len(uart_id, (size_t*)&length);
//
//  if ( length <  BOARD_RX_MAX_BUF_SIZE){
//
//		rxBytes = uart_read_bytes (uart_id, self.rx_buffer, length,0);
//
//		if (rxBytes > 0)
//			{
//				self.rx_buffer[rxBytes] = 0;
//				ESP_LOGI("BOARD_SERIAL_READ", "Read %d bytes: '%s'", rxBytes, self.rx_buffer);
//			}
//
//		else
//			{
//				ESP_LOGI("BOARD_SERIAL_READ", "No llego dato");
//			}
//
//		strcpy(buffer,(char*)self.rx_buffer);
//		printf("Leido:%#04x\r\n",(uint8_t)self.rx_buffer);
//		memset(self.rx_buffer,0,BOARD_RX_MAX_BUF_SIZE);
//		uart_flush(uart_id);
//		return rxBytes;
//  }
//
//  else
//    {
//      printf("RX BUFFER FULL");
//      return 0;
//    }

  rxBytes = uart_read_bytes((uart_port_t)uart_id, (uint8_t*)buffer, 1,0);
  //printf("Leido:%#04x\r\n",(uint8_t)buffer);

	return rxBytes;


}

uint32_t board_serial_read_until(board_uart_id_t uart_id ,void* buffer)
{

  int rxBytes = 0;
//  uint8_t Byte;
//  int length = 0;
//
//  uart_get_buffered_data_len(uart_id, (size_t*)&length);
//
//  if ( length <  BOARD_RX_MAX_BUF_SIZE){
//
//		rxBytes = uart_read_bytes (uart_id, self.rx_buffer, length,0);
//
//		if (rxBytes > 0)
//			{
//				self.rx_buffer[rxBytes] = 0;
//				ESP_LOGI("BOARD_SERIAL_READ", "Read %d bytes: '%s'", rxBytes, self.rx_buffer);
//			}
//
//		else
//			{
//				ESP_LOGI("BOARD_SERIAL_READ", "No llego dato");
//			}
//
//		strcpy(buffer,(char*)self.rx_buffer);
//		printf("Leido:%#04x\r\n",(uint8_t)self.rx_buffer);
//		memset(self.rx_buffer,0,BOARD_RX_MAX_BUF_SIZE);
//		uart_flush(uart_id);
//		return rxBytes;
//  }
//
//  else
//    {
//      printf("RX BUFFER FULL");
//      return 0;
//    }

  rxBytes = uart_read_bytes((uart_port_t)uart_id, (uint8_t*)buffer, 1,0);

	return rxBytes;


}


/**
 * @brief   Board Serial available data
 *
 * @param   uart_id UART port number
 *
 * @return
 *     - 1 Data Available
 *     - 0 No Data
 */
uint32_t board_serial_is_available (board_uart_id_t uart_id)
{

	uint32_t length = 0;

	switch (uart_id) {

		case BOARD_UART_0:
			if (uart_get_buffered_data_len ((uart_port_t)BOARD_UART_0, (size_t*) &length) == ESP_OK) {
				//Si length es mayor a cero hay datos disponibles
				if (length > 0) {
					return 1;
				}
				else {
					uart_flush ((uart_port_t)BOARD_UART_0);
					return 0;
				}
			}
			break;

		case BOARD_UART_1:

			if (uart_get_buffered_data_len ((uart_port_t)BOARD_UART_1, (size_t*) &length) == ESP_OK) {
				//Si length es mayor a cero hay datos disponibles
				if (length > 0) {
					ESP_LOGI("", "buffered data len %d:", length);
					return 1;
				}
				else {
					uart_flush ((uart_port_t)BOARD_UART_1);
					return 0;
				}
			}
			break;
		case BOARD_UART_2:
			if (uart_get_buffered_data_len ((uart_port_t)BOARD_UART_2, (size_t*) &length) == ESP_OK) {
				//Si length es mayor a cero hay datos disponibles
				if (length > 0) {
					return 1;
				}
				else {
					uart_flush ((uart_port_t)BOARD_UART_2);
					return 0;
				}
			}
			break;

		default:
			break;
	}
	return 0;
}


void board_serial_init(void)
{
	self.event_queue_0=NULL;
	self.event_queue_1=NULL;
	self.event_queue_2=NULL;


   Serial.begin(115200, SERIAL_8N1, 3, 1);

  //board_serial_setup(BOARD_UART_0);
  //board_serial_setup(BOARD_UART_1);
  //board_serial_setup(BOARD_UART_2);

  BOARD_LOG_TRACE_FUNCTION();

}
void board_serial_deinit(void){}
void board_serial_loop(void){}

/********************** end of file ******************************************/

/** @}Final Doxygen */
