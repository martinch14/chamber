#include <main.hpp>

#include "../../src/app_control/app_control.hpp"
#include "../../src/app_recepcion/app_recepcion.hpp"
#include "board.hpp"
#include "api.hpp"
#include "app_test.hpp"
#include "hardware.hpp"



void setup()
{
    board_init();
    api_init();


//    app_test_init();
    app_control_init();
    app_recepcion_init();

}

void loop()
{

#if 0
	static bool led_status=true;
	static int count = 1;

	if (count >= 0) {
		count--;
	}
	else {
		led_status=!led_status;
		board_gpio_write(BOARD_GPIOOUT_LED,led_status);
		count = 1;
	}

#endif

#if 0
	MONITOR_LOG("main loop");
#endif

	vTaskDelay(1000);
}
