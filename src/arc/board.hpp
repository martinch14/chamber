#ifndef BOARD_HPP_
#define BOARD_HPP_

/********************** inclusions *******************************************/

#include "Arduino.h"

#include <stdint.h>
#include <stdbool.h>

#include "board_delay.hpp"
#include "board_gpio.hpp"
#include "board_log.hpp"
#include "board_reset.hpp"
#include "board_serial.hpp"
#include "board_spiffs.hpp"
#include "board_timer.hpp"
#include "board_adc.hpp"
#include "board_dac.hpp"
#include "board_i2c.hpp"

/********************** macros ***********************************************/

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void board_init(void);

#endif /* BOARD_HPP_ */
/********************** end of file ******************************************/

