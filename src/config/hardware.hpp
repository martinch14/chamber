#ifndef _HARDWARE_HPP_
#define _HARDWARE_HPP_
/********************** inclusions *******************************************/

/********************** macros ***********************************************/

//___GPIO___
//INPUT

#define HARDWARE_ESP32_GPIOIN_3 		(33)
#define HARDWARE_ESP32_GPIOIN_4 		(14)
#define HARDWARE_ESP32_GPIOIN_5 		(4)

//OUTPUT
#define HARDWARE_ESP32_GPIO_0 			(2)
#define HARDWARE_ESP32_GPIO_7 			(25)
#define HARDWARE_ESP32_GPIO_6 			(26)


//___UART___
//UART 0
#define HARDWARE_ESP32_UART_0_RX 	(3)
#define HARDWARE_ESP32_UART_0_TX 	(1)
//UART 1
#define HARDWARE_ESP32_UART_1_RX 	(12)
#define HARDWARE_ESP32_UART_1_TX 	(15)


//__ADC__
#define BOARD_LAYOUT_ADC_0_ENABLE          (1)
#define BOARD_LAYOUT_ADC_0_PERIPH_ID       (0)
#define BOARD_LAYOUT_ADC_0_PIN             (34) //TEMP ADC

#define BOARD_LAYOUT_ADC_1_ENABLE          (0)
#define BOARD_LAYOUT_ADC_1_PERIPH_ID       (1)
#define BOARD_LAYOUT_ADC_1_PIN             (35)

#define BOARD_LAYOUT_ADC_READ_RESOLUTION   (12)
#define BOARD_LAYOUT_ADC_REFERENCE_VOLTAGE (3.3)
#define BOARD_LAYOUT_ADC_SETSAMPLES_ENABLE (0)


//__DAC__
#define BOARD_LAYOUT_DAC_VDD_REF (3.3)
/*obs: only 25 and 26 are DAC pins
 *
 * DAC1_ No funciona con codigo actual
 *
 * Valores   Out_Voltage
 *
 * 10		3.18
 * 80		2.27
 * 150		1.90
 * 210		2.64
 * 250		3.13
 *
 * DAC2_ Funciona OK
 *
 * 10		0.2
 * 80		1.05
 * 150		1.93
 * 210		2.66
 * 250		3.15
 *
 * */

#define BOARD_LAYOUT_DAC_1_PIN (25)
#define BOARD_LAYOUT_DAC_2_PIN (26)
#define BOARD_LAYOUT_DAC_1_ENABLE false
#define BOARD_LAYOUT_DAC_2_ENABLE false





/********************** BOARD I2C ****************************************/
//I2C
#define HARDWARE_ESP32_I2C_0_SDA	(18)//(21) //XXX: MODIFICACION POR PLACA DE SENSADO CON PINES CRUZADOS
#define HARDWARE_ESP32_I2C_0_SCL	(19)//(22)
#define HARDWARE_ESP32_I2C_0_MASTER_FREQ_HZ (100000)



/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

#endif /* _HARDWARE_HPP_ */










