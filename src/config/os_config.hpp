#ifndef _OS_CONFIG_HPP_
#define _OS_CONFIG_HPP_

/********************** TASKS CONFIG *******************************************/

//TASKS SIZES
#define OS_CONFIG_MOD_HEARTBEAT_TASK_SIZE									(4096)
#define OS_CONFIG_APP_CONTROL_TASK_SIZE										(4096)
#define OS_CONFIG_APP_RECEPCION_TASK_SIZE									(4096)

//TASKS PRIORITIES
#define OS_CONFIG_MOD_HEARTBEAT_TASK_PRIORITY								(2)
#define OS_CONFIG_APP_CONTROL_TASK_PRIORITY									(2)
#define OS_CONFIG_APP_RECEPCION_TASK_PRIORITY								(2)

//TASKS LOOP PERIODS
#define OS_CONFIG_MOD_HEARTBEAT_TASK_PERIOD									(1000)
#define OS_CONFIG_APP_CONTROL_TASK_PERIOD									(1000)
#define OS_CONFIG_APP_RECEPCION_TASK_PERIOD									(100)


//QUEUES
#define OS_CONFIG_MOD_HMI_RX_QUEUE_SIZE 									(5)

#define OS_CONFIG_THERMO_COMPLETE_PROGRAM_QUEUE_SIZE 						(1)
#define OS_CONFIG_THERMO_START_PROCESS_QUEUE_SIZE 							(1)
#define OS_CONFIG_THERMO_STOP_PROCESS_QUEUE_SIZE 							(1)




#define OS_CONFIG_APP_CONTROL_PARAMS_QUEUE_SIZE								(5)
#define OS_APP_CONTROL_STOP_QUEUE_SIZE										(1)

/********************** SYS CONFIG PARAMS ***************************************/

/********************** APPS CONFIG PARAMS **************************************/

/********************** end of file ******************************************/

#endif /* _OS_CONFIG_HPP_ */
