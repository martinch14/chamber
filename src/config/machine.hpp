/* 
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>  
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		machine.h
* @date		15 jun. 2021
* @author	
*		-Martin Abel Gambarotta   (magambarotta@gmail.com)
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

#ifndef COMPONENTS_CONFIG_INCLUDE_MACHINE_H_
#define COMPONENTS_CONFIG_INCLUDE_MACHINE_H_
/********************** inclusions *******************************************/

/********************** macros ***********************************************/

/* INFO*/

#define MACHINE_INFO_ID 									"THERSEAM_PRENSA_0001"
#define MACHINE_INFO_FIRMWARE_VERSION 		1
#define MACHINE_INFO_HARDWARE_VERSION 		1


/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

#endif /* COMPONENTS_CONFIG_INCLUDE_MACHINE_H_ */
/** @}Final Doxygen */

