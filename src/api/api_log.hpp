/*
 * Copyright (c) 2020 Sebastian Bedin <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @file	api_log.hpp
 * @date	2020-14-10
 * @author:
 *  - SB		Sebastian Bedin <sebabedin@gmail.com>
 * @version	v1.0.0
 *
 *	Date		Author		Modification
 * 	23-10		SB			Refactor and rename to .cpp
 */

#ifndef API_LOG_HPP_
#define API_LOG_HPP_

/********************** inclusions *******************************************/

#include <stdlib.h>
#include <stdio.h>
#include "logger.hpp"

/********************** macros ***********************************************/

#define API_LOG_ENABLE                      (0)
#define API_LOG_TRACE_ENABLE                (0)
#define API_LOG_TIMESTAMP_ENABLE            (0)
#define API_LOG_PRINT_BUFFER_LEN            (128)
#define API_LOG_HEAD                        ("/api/: ")
#define API_LOG_NL                          ("\n")

#define API_LOG_PRINT(...)\
    snprintf(g_api_log_print_buffer, API_LOG_PRINT_BUFFER_LEN,  __VA_ARGS__);\
    api_log_swrite(g_api_log_print_buffer)

#define API_LOG_PRINT_NL()                  API_LOG_PRINT(API_LOG_NL)

#if API_LOG_ENABLE && API_LOG_TIMESTAMP_ENABLE
#define API_LOG_PRINT_TIMESTAMP()           API_LOG_PRINT("[%06u] ", api_log_get_time_ms())
#else
#define API_LOG_PRINT_TIMESTAMP()
#endif

#define API_LOG_FORMAT(head, ...)\
    API_LOG_PRINT_TIMESTAMP();\
	API_LOG_PRINT(head);\
	API_LOG_PRINT(__VA_ARGS__);\
	API_LOG_PRINT_NL();

#if API_LOG_ENABLE
#define API_LOG(...)                        API_LOG_FORMAT(API_LOG_HEAD, __VA_ARGS__);
#else
#define API_LOG(...)
#endif

#if API_LOG_ENABLE && API_LOG_TRACE_ENABLE
#define API_LOG_TRACE_FUNCTION()            API_LOG("%s", __FUNCTION__);
#else
#define API_LOG_TRACE_FUNCTION(...)
#endif

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

extern char g_api_log_print_buffer[API_LOG_PRINT_BUFFER_LEN];

/********************** external functions declaration ***********************/

void api_log_swrite(char*);
unsigned int api_log_get_time_ms(void);

void api_log_init(void);


#endif /* API_LOG_HPP_ */
/********************** end of file ******************************************/
