#ifndef API_TIMER_HPP_
#define API_TIMER_HPP_

/********************** inclusions *******************************************/

#include <stdlib.h>
#include <stdint.h>

/********************** macros ***********************************************/

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void api_timer_blocking_delay_ms(uint32_t time_ms);
uint32_t api_timer_get_walltime_ms(void);

void api_timer_init(void);

#endif /* API_TIMER_HPP_ */
/********************** end of file ******************************************/

