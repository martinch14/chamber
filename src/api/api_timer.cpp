/********************** inclusions *******************************************/

#include <stdlib.h>
#include <stdint.h>
#include <board.hpp>
#include <api_log.hpp>
#include <api_timer.hpp>

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/

void api_timer_blocking_delay_ms(uint32_t time_ms)
{
	board_timer_delay_ms_blocking(time_ms);
}

uint32_t api_timer_get_walltime_ms(void)
{
    return board_timer_get_time_ms();
}

void api_timer_init(void)
{
	API_LOG_TRACE_FUNCTION();
}

/********************** end of file ******************************************/

