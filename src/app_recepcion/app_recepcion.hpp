#ifndef APP_RECEPCION_HPP_
#define APP_RECEPCION_HPP_

/********************** inclusions *******************************************/
#include "board.hpp"
#include "os_config.hpp"
#include "logger.hpp"

/********************** macros ***********************************************/

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void app_recepcion_init(void);
void app_recepcion_deinit(void);
void app_recepcion_task_loop(void *pvParameters);

#endif /* APP_RECEPCION_HPP_ */
/********************** end of file ******************************************/
