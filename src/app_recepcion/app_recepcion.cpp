/********************** inclusions *******************************************/

#include "app_recepcion.hpp"

#include "api_serial.hpp"
#include "board_serial.hpp"
#include "HardwareSerial.h"
#include "driver/uart.h"
#include "app_control.hpp"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

static app_control_params_t   params;

/********************** external data definition *****************************/

/********************** internal functions definition ************************/
/********************** external functions definition ************************/



void app_recepcion_init(void) {

	xTaskCreate(app_recepcion_task_loop,
			"recepcion task", 				// A name just for humans
			OS_CONFIG_APP_RECEPCION_TASK_SIZE, // This stack size can be checked & adjusted by reading the Stack Highwater
			NULL,
			OS_CONFIG_APP_RECEPCION_TASK_PRIORITY,							 // Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
			NULL);



}

void app_recepcion_deinit(void) {

}

void app_recepcion_task_loop(void *pvParameters) {

	String input;
	int len;
	uint8_t* buf;
	uint8_t* data = (uint8_t*) malloc(256);



	bool dummy_send = true;

	for (;;) {
		if (Serial.available()) {
			// Si hay datos disponibles en el puerto Serial2

			len= uart_read_bytes(UART_NUM_0,buf, 256,0);



			input = Serial.readStringUntil('\n');
			// Se lee la cadena de caracteres enviada por el puerto serial hasta
			// encontrar el caracter '\n' (salto de línea)
			input.trim(); // Se eliminan los espacios en blanco al principio y al final de la cadena

			if (input.startsWith("hum ")) {

#if 0
		      // Si la cadena empieza con "CMD1", se asume que la parte variable es un número
		      int value = self_.input.substring(4).toInt(); // Se obtiene la parte variable de la cadena y se convierte a un número entero
		      Serial.print("Valor recibido: ");
		      Serial.println(value);
		      // Se envía una respuesta al puerto serial con el valor recibido
#endif



		      	params.target_rh = input.substring(4).toInt();
		      	Serial.printf("chamber_console_targetrh %d \n", params.target_rh);
				xQueueGenericSend(app_control_params_queue, &params, 0U, 0U);

				board_gpio_write(BOARD_GPIOOUT_LED, true);

			}
			else if (input.startsWith("stop")) {
#if 0
		    	// Si la cadena empieza con "CMD2", se asume que la parte variable es una cadena de caracteres
		      String value = inputString.substring(4); // Se obtiene la parte variable de la cadena
		      Serial.print("Texto recibido: ");
		      Serial.println(value);
		      // Se envía una respuesta al puerto serial con el texto recibido
#endif


		      Serial.printf("chamber control stopped!\n");
		      xQueueGenericSend(app_control_stop_queue, &dummy_send, 0U, 0U);
		      board_gpio_write(BOARD_GPIOOUT_LED, false);
			}
			else {
				Serial.println("Comando no reconocido");
				// Si la cadena no empieza ni con "CMD1" ni con "CMD2", se envía una respuesta de error
			}
		}

		//DEBUG_LOG("app_recepcion!");
		vTaskDelay(OS_CONFIG_APP_RECEPCION_TASK_PERIOD);
	}
}



/********************** end of file ******************************************/
