/*
 * Copyright (c) 2020 Sebastian Bedin <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @file	cos_log.hpp
 * @date	Oct 25, 2020
 * @author:
 *  - SB		Sebastian Bedin <sebabedin@gmail.com>
 * @version	v1.0.0
 *
 *	Date			Author		Modification
 * 	Oct 25, 2020	SB			Init
 */

#ifndef LOGGER_HPP_
#define LOGGER_HPP_

/********************** inclusions *******************************************/

#include <stdio.h>
#include "api_log.hpp"

/********************** macros ***********************************************/

#define DEBUG_LOG_ENABLE        (1)
#define TRACE_LOG_ENABLE        (0)
#define MONITOR_LOG_ENABLE      (0)
#define ERROR_LOG_ENABLE        (0)

#if 1 == DEBUG_LOG_ENABLE
#define DEBUG_LOG(...)              API_LOG_FORMAT("/deb/;", __VA_ARGS__)
#else
#define DEBUG_LOG(...)
#endif
#define DEBUG_LOG_FUCTION()         DEBUG_LOG(__FUNCTION__)

#if 1 == TRACE_LOG_ENABLE
#define TRACE_LOG(...)              API_LOG_FORMAT("/trc/;", __VA_ARGS__)
#else
#define TRACE_LOG(...)
#endif
#define TRACE_LOG_FUCTION()         TRACE_LOG(__FUNCTION__)

#if 1 == MONITOR_LOG_ENABLE
#define MONITOR_LOG(...)            API_LOG_FORMAT("/mot/;", __VA_ARGS__)
#else
#define MONITOR_LOG(...)
#endif
#define MONITOR_LOG_FUCTION()       MONITOR_LOG(__FUNCTION__)

#if 1 == ERROR_LOG_ENABLE
#define ERROR_LOG(...)              API_LOG_FORMAT("/err/;", __VA_ARGS__)
#else
#define ERROR_LOG(...)
#endif
#define ERROR_LOG_FUCTION()         ERROR_LOG(__FUNCTION__)

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

#endif /* LOGGER_HPP_ */
/********************** end of file ******************************************/

