/********************** inclusions *******************************************/

#include "app_test.hpp"

#include "mod_heartbeat.hpp"


/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/
/********************** external functions definition ************************/


void app_test_init(void) {

#if 1
	mod_heartbeat_init();
#endif
}

void app_test_deinit(void) {

}

void app_test_task_loop(void *pvParameters) {

}



/********************** end of file ******************************************/
