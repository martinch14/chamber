#ifndef APP_TEST_HPP_
#define APP_TEST_HPP_

/********************** inclusions *******************************************/

/********************** macros ***********************************************/

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void app_test_init(void);
void app_test_deinit(void);
void app_test_task_loop(void *pvParameters);

#endif /* APP_TEST_HPP_ */
/********************** end of file ******************************************/
