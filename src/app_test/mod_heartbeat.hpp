#ifndef MOD_HEARTBEAT_HPP_
#define MOD_HEARTBEAT_HPP_

/********************** inclusions *******************************************/
#include "board.hpp"
#include "os_config.hpp"
#include "logger.hpp"
#include "api.hpp"
/********************** macros ***********************************************/

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void mod_heartbeat_init(void);
void mod_heartbeat_deinit(void);
void mod_heartbeat_task_loop(void *pvParameters);

#endif /* MOD_HEARTBEAT_HPP_ */
/********************** end of file ******************************************/
