 /********************** inclusions *******************************************/

#include "mod_heartbeat.hpp"


/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/


void mod_heartbeat_init(void) {

	xTaskCreate(mod_heartbeat_task_loop,
			"mod heartbeat task", 				// A name just for humans
			OS_CONFIG_MOD_HEARTBEAT_TASK_SIZE, // This stack size can be checked & adjusted by reading the Stack Highwater
			NULL,
			OS_CONFIG_MOD_HEARTBEAT_TASK_PRIORITY,							 // Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
			NULL);

}

void mod_heartbeat_deinit(void) {

}

void mod_heartbeat_task_loop(void *pvParameters) {

	for (;;) {


		vTaskDelay(OS_CONFIG_MOD_HEARTBEAT_TASK_PERIOD);
	}
}



/********************** end of file ******************************************/
