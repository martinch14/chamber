#ifndef APP_CONTROL_HPP_
#define APP_CONTROL_HPP_

/********************** inclusions *******************************************/
#include "board.hpp"
#include "os_config.hpp"
#include "logger.hpp"
#include "api.hpp"
/********************** macros ***********************************************/

#define APP_CONTROL_EXPERIMENT_1_ENABLE (true) //for testing

//#define APP_CONTROL_HUMIFIER_PIN HARDWARE_ESP32_GPIO_6
//#define APP_CONTROL_DRYER_PIN HARDWARE_ESP32_GPIO_7

#define APP_CONTROL_USE_DEFAULT_RH_GAPS 1
//UPPER LIMIT --> 		ACTUAL > (TARGET + (NORM_UPPER_GAP - TARGET * FACTOR))
#define APP_CONTROL_UPPER_NORM_GAP_RH_DEFAULT 1
#define APP_CONTROL_UPPER_FACTOR_RH_DEFAULT 0
//LOWER LIMIT --> 		ACTUAL < (TARGET - (NORM_LOWER_GAP - TARGET * FACTOR))
#define APP_CONTROL_LOWER_NORM_GAP_RH_DEFAULT 1
#define APP_CONTROL_LOWER_FACTOR_RH_DEFAULT 0

#define APP_CONTROL_HUMIFIER_ENABLE_RANGE 2 //if((self_.params.target_rh >= self_.ambient_rh) || (self_.params.target_rh- self_.actual_rh > APP_CONTROL_HUMIFIER_ENABLE_RANGE))
#define APP_CONTROL_DRYER_ENABLE_RANGE 2 //if((self_.params.target_rh < self_.ambient_rh) || ( self_.actual_rh - self_.params.target_rh > APP_CONTROL_DRYER_ENABLE_RANGE))

/********************** typedef **********************************************/

typedef struct{

	int target_rh;

	float upper_norm_gap_rh;
	float lower_norm_gap_rh;

	float upper_factor_rh;
	float lower_factor_rh;

}app_control_params_t;

typedef enum{

	APP_CONTROL_CHAMBER_OFF = 0,
	APP_CONTROL_CHAMBER_HUMIDIFYING,
	APP_CONTROL_CHAMBER_DRYINGUP,
	APP_CONTROL_CHAMBER_BALANCED,

}app_control_status_t;

/********************** external data declaration ****************************/

extern QueueHandle_t app_control_params_queue;
extern QueueHandle_t app_control_stop_queue;

/********************** external functions declaration ***********************/

void app_control_init(void);
void app_control_deinit(void);
void app_control_task_loop(void *pvParameters);

#endif /* APP_CONTROL_HPP_ */
/********************** end of file ******************************************/
