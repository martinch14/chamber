/********************** inclusions *******************************************/

#include "app_control.hpp"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

static struct{

	app_control_params_t params;
	app_control_status_t status;


	double actual_rh;
	double ambient_rh;
	bool dummy;
	int dry_ena;
	int hum_ena;


}self_;

#if true == APP_CONTROL_EXPERIMENT_1_ENABLE

static struct{

	uint32_t id;

	uint32_t aux_test_counter;

	bool actual_dryer_state;
	bool actual_humidifier_state;

	uint32_t dryer_on_counter;
	uint32_t humidifier_on_counter;

	uint32_t dryer_off_counter;
	uint32_t humidifier_off_counter;

}self_testing_;
#endif


/********************** external data definition *****************************/

QueueHandle_t app_control_params_queue = NULL;
QueueHandle_t app_control_stop_queue = NULL;

/********************** internal functions definition ************************/

static double get_ambient_rh_(void){

	double aux;
	aux = api_bosch_bme280_get_humidity(API_BOSCH_BME280_TAG_2);;

	if ( 0 != aux ) {
		return aux;
	}
	else {
		return  self_.ambient_rh;
	}

}

static double get_actual_rh_(void){


	double aux;
	aux = api_bosch_bme280_get_humidity(API_BOSCH_BME280_TAG_1);;
	if (0 != aux ) {
		return aux;
	}
	else {
		return  self_.actual_rh;
	}
	//return api_bosch_bme280_get_humidity(API_BOSCH_BME280_TAG_1);
}

static void humidifier_enable_(bool enable ){

	if((self_.params.target_rh >= self_.ambient_rh) || (self_.params.target_rh- self_.actual_rh > APP_CONTROL_HUMIFIER_ENABLE_RANGE)){

		board_gpio_write(BOARD_GPIOOUT_WET_VALVE, enable);
		self_.hum_ena=1;
	}
	else{
		board_gpio_write(BOARD_GPIOOUT_WET_VALVE, false);
		self_.hum_ena=0;
	}


}

static void dryer_enable_(bool enable){

	if((self_.params.target_rh < self_.ambient_rh) || ( self_.actual_rh - self_.params.target_rh > APP_CONTROL_DRYER_ENABLE_RANGE)){

		board_gpio_write(BOARD_GPIOOUT_DRY_VALVE, enable);
		self_.dry_ena=1;
	}
	else{
		board_gpio_write(BOARD_GPIOOUT_DRY_VALVE, false);
		self_.dry_ena=0;
	}


}


/********************** external functions definition ************************/


void app_control_init(void) {

	xTaskCreate(app_control_task_loop,
			"mod heartbeat task", 				// A name just for humans
			OS_CONFIG_APP_CONTROL_TASK_SIZE, // This stack size can be checked & adjusted by reading the Stack Highwater
			NULL,
			OS_CONFIG_APP_CONTROL_TASK_PRIORITY,							 // Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
			NULL);
}

void app_control_deinit(void) {

}

void app_control_task_loop(void *pvParameters) {


#if true == APP_CONTROL_EXPERIMENT_1_ENABLE
	self_testing_.id =0;
	self_testing_.aux_test_counter=0;
	self_testing_.dryer_on_counter=0;
	self_testing_.humidifier_on_counter=0;
	self_testing_.actual_dryer_state=false;
	self_testing_.actual_humidifier_state=false;
#endif
	//INIT STATUS OFF
    self_.status=APP_CONTROL_CHAMBER_OFF;

    //CREATE PARAMS QUEUE
	app_control_params_queue = xQueueCreate( OS_CONFIG_APP_CONTROL_PARAMS_QUEUE_SIZE , sizeof( app_control_params_t) );
    //CREATE STOP QUEUE
	app_control_stop_queue = xQueueCreate( OS_APP_CONTROL_STOP_QUEUE_SIZE , sizeof( bool) );




	for (;;) {

   if (xQueueReceive( app_control_params_queue, &self_.params, portMAX_DELAY) == pdTRUE ){

	    	 printf("chamber_status START,%d \n", self_.params.target_rh);


			 self_.status=APP_CONTROL_CHAMBER_BALANCED;

	    	 if(true == APP_CONTROL_USE_DEFAULT_RH_GAPS){
	    		 self_.params.upper_norm_gap_rh =APP_CONTROL_UPPER_NORM_GAP_RH_DEFAULT;
	    		 self_.params.lower_norm_gap_rh =APP_CONTROL_LOWER_NORM_GAP_RH_DEFAULT;
	    		 self_.params.upper_factor_rh =APP_CONTROL_UPPER_FACTOR_RH_DEFAULT;
	    		 self_.params.lower_factor_rh =APP_CONTROL_LOWER_FACTOR_RH_DEFAULT;
	    		 self_.dry_ena=0;
	    		 self_.hum_ena=0;
	    	 }


	    	 if(NULL != app_control_stop_queue){
	 	    	while(xQueueReceive( app_control_stop_queue, &self_.dummy, 0) != pdTRUE ){

	 				 vTaskDelay(OS_CONFIG_APP_CONTROL_TASK_PERIOD);


#if 0
	 				 /*Control Principal Disable para testing*/

	 				 self_.actual_rh=get_actual_rh_();
	 				 self_.ambient_rh=get_ambient_rh_();


	 				 if(APP_CONTROL_CHAMBER_BALANCED == self_.status){

	 					 //ACTUAL > (TARGET + (NORM_UPPER_GAP - TARGET * FACTOR))
	 					 if(self_.actual_rh>(self_.params.target_rh+(self_.params.upper_norm_gap_rh - (self_.params.target_rh * (self_.params.upper_factor_rh))))){
	 	 					self_.status=APP_CONTROL_CHAMBER_DRYINGUP;
	 	 					humidifier_enable_(false);
	 	 					dryer_enable_(true);
	 					 }

	 					 //ACTUAL < (TARGET - (NORM_LOWER_GAP - TARGET * FACTOR))
	 					 if(self_.actual_rh < (self_.params.target_rh-(self_.params.lower_norm_gap_rh - (self_.params.target_rh * (self_.params.lower_factor_rh))))){
	 	 					self_.status=APP_CONTROL_CHAMBER_HUMIDIFYING;
	 	 					humidifier_enable_(true);
	 	 					dryer_enable_(false);
	 					 }

	 				 }

	 				 else if(APP_CONTROL_CHAMBER_DRYINGUP == self_.status){

	 					 if(self_.actual_rh< (self_.params.target_rh)){
	 	 					self_.status=APP_CONTROL_CHAMBER_BALANCED;
	 	 					humidifier_enable_(false);
	 	 					dryer_enable_(false);
	 					 }


	 				 }

	 				 else if(APP_CONTROL_CHAMBER_HUMIDIFYING == self_.status){

	 					 if(self_.actual_rh > (self_.params.target_rh)){
		 	 					humidifier_enable_(false);
		 	 					dryer_enable_(false);
	 	 					self_.status=APP_CONTROL_CHAMBER_BALANCED;
	 					 }

	 				 }

#endif


#if 1
	 				 /* Control para Testing**/

	 				 self_.actual_rh=40;
	 				 self_.ambient_rh=90;

#endif




#if true == APP_CONTROL_EXPERIMENT_1_ENABLE

				/*******Logging for Spyder********/
				printf("camara_exp_1 %lld,%lf,%lf \n",esp_timer_get_time()/1000000,self_.ambient_rh,self_.actual_rh);
#endif


	 	    	}

	    	 }
	       self_.status=APP_CONTROL_CHAMBER_OFF;
	       board_gpio_write(BOARD_GPIOOUT_WET_VALVE, false);
	       board_gpio_write(BOARD_GPIOOUT_DRY_VALVE, false);
	       printf("chamber_status STOP \n");

	       }

		DEBUG_LOG("app_control!");
		vTaskDelay(OS_CONFIG_APP_CONTROL_TASK_PERIOD);
	}
}






/********************** end of file ******************************************/
